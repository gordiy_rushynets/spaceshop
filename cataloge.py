import os
from goods.models import *

def path_to_photo(image_id):
    image = Image.objects.get(id=image_id)
    path = os.getcwd() + '/' + str(image.goods_image)
    return path

def catalog():
    goods = Goods.objects.all()

    array_of_goods = []

    print(goods)

    for i in range(len(goods)):
        item = {}
        item['id'] = goods[i].id
        item['goods'] = goods[i].goods
        item['count_of_goods'] = goods[i].count_of_goods
        item['sale_price'] = goods[i].sale_price
        item['description'] = goods[i].description
        item['category'] = goods[i].category.category
        item['images'] = []

        images = Image.objects.all().filter(goods=goods[i])

        for j in range(len(images)):
            image = path_to_photo(images[j].id)
            item['images'].append(image)

        array_of_goods.append(item)

    return array_of_goods



