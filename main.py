import time
import telebot
from settings import token
from database import MySQL

bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def send_welcome(message):
    user_murkup = telebot.types.ReplyKeyboardMarkup(True)
    user_murkup.row('Купить', 'Посмотреть товари')

    msg = bot.send_message(message.from_user.id, "Привет",reply_markup=user_murkup)
    bot.register_next_step_handler(msg, menu)


def menu(message):
    if message.text == "Купить":
        pass
    elif message.text == 'Посмотреть товари':
        templates = ""
    else:
        user_murkup = telebot.types.ReplyKeyboardMarkup(True)
        user_murkup.row('/start')

        msg = bot.send_message(message.from_user.id, "Начните с начала.", reply_markup=user_murkup)
        bot.register_next_step_handler(msg, send_welcome)


bot.skip_pending = True

while True:

    try:

        bot.polling(none_stop=True)

    # ConnectionError and ReadTimeout because of possible timout of the requests library

    # TypeError for moviepy errors

    # maybe there are others, therefore Exception

    except Exception as e:


        time.sleep(15)
