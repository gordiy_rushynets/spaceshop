import mysql.connector


class MySQL:
    def __init__(self, host, database_login, database_password):
        self.host = host
        self.db_login = database_login
        self.db_password = database_password

    def create_database(self, database_name):
        """
        create_database function must create database in MySQL
        :param database_name:
        :return: 0 if all is Okay, 1 if database created before
        """
        self.db = mysql.connector.connect(
            host=self.host,
            user=self.db_login,
            passwd=self.db_password
        )

        self.cursor = self.db.cursor()

        try:
            self.cursor.execute("CREATE DATABASE {0}".format(database_name))
            return 0
        except Exception as e:
            self.cursor.execute("SHOW DATABASES")

            for db in self.cursor:
                if db == database_name:
                    print("Database created")
                    break
            return 1

    def connect_to_database(self, database_name):
        """
        function must connect to database which created before
        :param database_name: str
        :return: 0 if connection success, 1 if can not connect to db
        """
        try:
            self.db = mysql.connector.connect(
                host=self.host,
                user=self.db_login,
                passwd=self.db_password,
                database = database_name
            )
        except Exception as e:
            print(e, "Can not connect to database. Check even if database created before.")
            return 1

        self.cursor = self.db.cursor()
        return 0

    def create_table(self, table_name, **kwargs):
        """
        must create table in database with certain fields
        :param table_name: str
        :param kwargs: Set data about arguments in format: variable_name=variable_type(data type). Available data types: INT, VARCHAR, DECIMAL, DATETIME, BLOB(files), FOREIGN KEY.
        :return: 0 if all is Okay, 1 if can not create table
        """

        variables = ''

        for arg_name, arg_type in kwargs.items():

            if arg_type == "INT":
                argument = arg_name + ' INT,'
            elif arg_type == "VARCHAR":
                argument = arg_name + " VARCHAR(255),"
            elif arg_type == "DECIMAL":
                argument = arg_name + " DECIMAL,"
            elif arg_type == "DATETIME":
                argument = arg_name + " DATETIME,"
            elif arg_type == 'LONGBLOB':
                argument = arg_name + " LONGBLOB,"
            elif arg_type == "FOREIGN KEY":
                argument = arg_name + "_id INTEGER, FOREIGN KEY({0}_id) REFERENCES {1}(Id),".format(arg_name, arg_name)

            variables += argument

        variables = variables[:-1]

        try:
            self.cursor.execute("CREATE TABLE {0} ({1})".format(table_name, variables))
        except Exception as e:
            print(e, "Available data types: INT, VARCHAR, DECIMAL, DATETIME, FOREIGN KEY. Set data about arguments in format: variable_name=variable_type(data type).")
            return 1

        return 0

    def insert_data(self, table_name, where=None, **kwargs):

        """
        this function must save data to database
        :param table_name: name of table in db
        :param kwargs: variables and values in database
        :param where: default=None, where={
            "name_of_table_for_linking": "str",
            "id": id
        }
        :return: None
        """

        arguments = ''
        values = []
        line_values = ''
        for arg_name, arg_val in kwargs.items():
            arguments += arg_name + ', '
            values.append(arg_val)

            line_values += '%s,'

        # remove last coma
        line_values = line_values[:-1]

        # remove last gap and coma
        arguments = arguments[:-2]

        # convert to tuple
        values = tuple(values)

        if where is None:
            sql = "INSERT INTO {0} ({1}) VALUES ({2})".format(table_name, arguments, line_values)

            self.cursor.execute(sql, values)
            self.db.commit()
        elif isinstance(where, dict):
            sql = "INSERT INTO {0} ({1}) VALUES ({2}, (SELECT id FROM {3} WHERE id = {4}))".format(table_name, arguments, line_values, where['name_of_table_for_linking'], where['id'])

            self.cursor.execute(sql, values)
            self.db.commit()

    def select_data(self, *args, table_name):
        if len(args) == 0:
            self.cursor.execute("SELECT * FROM {0}".format(table_name))
            result = self.cursor.fetchall()
            return result
        else:
            # create query
            query = "SELECT "
            for arg in args:
                query += arg + ', '

            query = query[:-2]

            query = query + ' FROM ' + table_name

            self.cursor.execute(query)
            result = self.cursor.fetchall()
            return result
