from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Provider(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=150, default=None)
    surname = models.CharField(max_length=150, default=None)
    region = models.CharField(max_length=150, default=None)
    name_of_store = models.CharField(max_length=150, default=None)
    phone = PhoneNumberField(null=False, blank=False, unique=False)
    instagram = models.CharField(max_length=150)

    class Meta:
        verbose_name='Постачальник'
        verbose_name_plural='Постачальники'

    def __str__(self):
        return 'Постачальник: {0} {1}. Instagram: {2}'.format(self.surname, self.name, self.instagram)


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=150)

    class Meta:
        verbose_name = "Категорія"
        verbose_name_plural = "Категорії"

    def __str__(self):
        return 'Категорія: {0}'.format(self.category)

class Goods(models.Model):
    id = models.AutoField(primary_key=True)
    goods = models.CharField(max_length=150)
    count_of_goods = models.IntegerField(default=0)
    purchase_price = models.IntegerField(default=0)
    sale_price = models.IntegerField(default=0)
    description = models.TextField()
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товари"

    def __str__(self):
        return '{0} Категорія: {1}, Закупочна ціна: {2}, Ціна на продажу: {3}'.format(self.goods, self.category.category, self.purchase_price, self.sale_price)


class Image(models.Model):
    id = models.AutoField(primary_key=True)
    goods_image = models.ImageField(upload_to='images')
    goods = models.ForeignKey(Goods, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Фото товару"
        verbose_name_plural = "Фото товарів"

    def __str__(self):
        # return 'Товар: {}'.format(self.goods.goods)
        return 'id: '+str(self.id)


class Status(models.Model):
    id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=150)

    class Meta:
        verbose_name = "Стан замовлення"
        verbose_name_plural = "Стани замовленнь"



class Order(models.Model):
    id = models.AutoField(primary_key=True)
    goods = models.ForeignKey(Goods, on_delete=models.CASCADE)
    count = models.IntegerField(default=0)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)

    def sale_price(self):
        return self.goods.sale_price * self.count

    def purchase_price(self):
        return self.goods.purchase_price * self.count

    class Meta:
        verbose_name = "Замовлення"
        verbose_name_plural = "Замовлення"

    def __str__(self):
        return 'Замовлено {0}, ціна замовлення {1}'.format(self.goods.goods, self.sale_price())
